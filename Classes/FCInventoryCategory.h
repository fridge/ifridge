#import <Foundation/Foundation.h>

@interface FCInventoryCategory : NSObject {
    int displaySequence;
    NSString *title;
    NSMutableArray *products;
}

@property (nonatomic, assign) int displaySequence;
@property (nonatomic, retain) NSString *title;
@property (readonly) NSMutableArray *products;

- (NSComparisonResult)compareDisplaySequence:(FCInventoryCategory*)other;

@end
