#import <Foundation/Foundation.h>

#import "FCXMLRPC.h"

#import "FCXMLRPCRequest.h"
#import "FCProtocolHelper.h"

enum FCAuthenticatedRequestState {
    FCAuthenticatedRequestStateNonce,
    FCAuthenticatedRequestStateInner
};


@interface FCAuthenticatedRequest : NSObject<FCXMLRPCRequest, FCXMLRPCEndpointDelegate> {
    FCXMLRPCRequest *nonceRequest;
    FCXMLRPCRequest *innerRequest;
    FCProtocolHelper *helper;
    NSString *clientNonce;
    NSString *serverNonce;
    enum FCAuthenticatedRequestState state;
    id<FCXMLRPCEndpointDelegate> delegate;
}

@property (readonly) NSString *serverNonce;

@property (nonatomic, retain) FCXMLRPCRequest *nonceRequest;
@property (nonatomic, retain) FCXMLRPCRequest *innerRequest;


- (id)initWithMethodName:(NSString*)aMethodName
                  params:(NSArray*)someParams
          protocolHelper:(FCProtocolHelper*)aHelper
                delegate:(id)aDelegate;

@end
