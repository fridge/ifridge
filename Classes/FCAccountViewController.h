#import <UIKit/UIKit.h>

#import "FCXMLRPC.h"

@class FCProtocolHelper;
@class FCFridgeViewController;

@interface FCAccountViewController : UIViewController<FCXMLRPCEndpointDelegate> {
    FCFridgeViewController *fridgeViewController;
    FCProtocolHelper *endpoint;
    IBOutlet UILabel *balanceLabel;
}

@property (nonatomic, assign) FCFridgeViewController *fridgeViewController;

@end
