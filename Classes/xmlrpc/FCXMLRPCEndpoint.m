#import "FCXMLRPCEndpoint.h"

#import "FCXMLRPCRequest.h"

@implementation FCXMLRPCEndpoint

@synthesize endpointURL;
@synthesize delegate;


- (id)initWithEndpointURL:(NSURL*)anEndpointURL delegate:(id)aDelegate {
    if (self = [super init]) {
        endpointURL = [anEndpointURL retain];
        self.delegate = aDelegate;
    }
    return self;
}

- (FCXMLRPCRequest*) sendRequestForMethod:(NSString*)methodName
                               withParams:(NSArray*)params {
    FCXMLRPCRequest *request = [self requestForMethod:methodName withParams:params];
    [request send];
    return request;
}

- (FCXMLRPCRequest*) requestForMethod:(NSString*)methodName
                           withParams:(NSArray*)params {

    return [[[FCXMLRPCRequest alloc] initWithURL:endpointURL
                                      methodName:methodName
                                          params:params
                                        delegate:delegate] autorelease];
}

- (void)dealloc {
    [endpointURL release];
    endpointURL = nil;

    [super dealloc];
}



@end
