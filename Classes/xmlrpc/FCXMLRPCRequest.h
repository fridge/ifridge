#import <Foundation/Foundation.h>

#import "DDXML.h"
#import "FCXMLRPCEndpointDelegate.h"

@class FCXMLRPCEndpoint;

@protocol FCXMLRPCRequest

@property (nonatomic, retain) NSString *methodName;
@property (nonatomic, retain) NSMutableArray *params;

@property (nonatomic, assign) id<FCXMLRPCEndpointDelegate> delegate;

- (void)send;
- (void)cancel;

@end


@interface FCXMLRPCRequest : NSObject<FCXMLRPCRequest> {
    NSURLConnection *urlConnection;

    NSMutableArray *params;
    NSString *methodName;

    NSURL *url;

    NSMutableData *responseBuffer;
    id<FCXMLRPCEndpointDelegate> delegate;

    // Parser state
    BOOL faulted;
    BOOL acceptingValue;
    NSMutableArray *parsingContexts;
    NSMutableString *stringBuilder;

    id response;
}

@property (nonatomic, retain) NSURL *url;

- (NSData*) encodeRequest;

- (void)requestTerminated;

- (id)initWithURL:(NSURL*)aURL methodName:(NSString*)aMethodName params:(NSArray*)someParams delegate:(id)aDelegate;
@end
