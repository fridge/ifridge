#import "FCXMLRPCRequest.h"

#import "FCXMLRPCEndpoint.h"

@implementation FCXMLRPCRequest

static id kFCXMLRPCRequest_ArrayStart = @"kFCXMLRPCRequest_ArrayStart";
static id kFCXMLRPCRequest_StructStart = @"kFCXMLRPCRequest_StructStart";
static id kFCXMLRPCRequest_StructName = @"kFCXMLRPCRequest_StructName";

@synthesize url, delegate;
@synthesize methodName, params;

- (id)initWithURL:(NSURL*)aURL methodName:(NSString*)aMethodName params:(NSArray*)someParams delegate:(id)aDelegate {
    if (self = [super init]) {
        self.url = aURL;
        self.delegate = aDelegate;

        self.methodName = aMethodName;
        self.params = [someParams mutableCopy];
    }
    return self;
}


- (void)send {
    [self retain];

    if (urlConnection) {
        [NSException raise:@"FCXMLRPCRequestAlreadyStarted"
                    format:@"An attempt was made to start a request that was already in progress"];
    }

    responseBuffer = [[NSMutableData alloc] init];

    NSMutableURLRequest *urlRequest = [[NSMutableURLRequest alloc] init];
    urlRequest.URL = url;
    [urlRequest addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    urlRequest.HTTPBody = [self encodeRequest];
    urlRequest.HTTPMethod = @"POST";

    urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
    [urlRequest release];
}

- (void)cancel {
    [urlConnection cancel];
}

- (void)requestTerminated {
    responseBuffer = ([responseBuffer release], nil);
    response = ([response release], nil);
}

- (void)dealloc {
    // Stop events going off first
    delegate = nil;

    [urlConnection cancel];
    urlConnection = ([urlConnection release], nil);

    // TODO: It's quite likely these will always be nil after [urlConnection cancel]
    responseBuffer = ([responseBuffer release], nil);
    response = ([response release], nil);

    [super dealloc];
}

#pragma mark -
#pragma mark Body Encoding

- (DDXMLElement*)marshalParam:(id)param {
    DDXMLElement *value = [DDXMLElement elementWithName:@"value"];
    DDXMLElement *sub = nil;
    if ([param isKindOfClass:[NSString class]]) {
        sub = [DDXMLElement elementWithName:@"string"];
        [sub addChild:[DDXMLElement textWithStringValue:param]];
    }
    else if ([param isKindOfClass:[NSNumber class]]) {
        sub = [DDXMLElement elementWithName:@"int"];
        [sub addChild:[DDXMLElement textWithStringValue:[param stringValue]]];
    }
    else {
        [NSException raise:@"FCXMLRPCMarshallingUnknownType"
                    format:@"Object cannot be trivially marshalled:%@", param];
        return nil;
    }
    [value addChild:sub];
    return value;
}

- (NSData*) encodeRequest {
    DDXMLElement *rootNode = [DDXMLElement elementWithName:@"methodCall"];

    [rootNode addChild:[DDXMLElement elementWithName:@"methodName"
                                         stringValue:methodName]];

    if (params) {
        DDXMLElement *paramsNode = [DDXMLElement elementWithName:@"params"];

        [rootNode addChild:paramsNode];

        for (id param in params) {
            DDXMLElement *paramNode = [DDXMLElement elementWithName:@"param"];
            [paramNode addChild:[self marshalParam:param]];
            [paramsNode addChild:paramNode];
        }
    }

    NSData *xmlData = [[rootNode XMLString] dataUsingEncoding:NSUTF8StringEncoding];
    return xmlData;
}

#pragma mark -
#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // There's no streaming input source to NSXMLParser, so we buffer.
    [responseBuffer appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)aConnection {
    // parse result
    NSXMLParser *parser = [[NSXMLParser alloc] initWithData:responseBuffer];
    parser.delegate = self;
    [parser parse];
    [parser release];

    if (response) {
        if (faulted) {
            [delegate requestFaulted:self withResult:response];
        }
        else {
            [delegate requestFinished:self withResult:response];
        }
    }

    [self requestTerminated];
    [self autorelease];
}

- (void)connection:(NSURLConnection*)connection didFailWithError:(NSError*)error {
    [delegate requestFailed:self withError:error];
    [self requestTerminated];
    [self autorelease];
}

#pragma mark -
#pragma mark NSXMLParser Delegate Methods

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict {
    if ([elementName isEqualToString:@"methodResponse"]) {
        [parsingContexts release];
        parsingContexts = [[NSMutableArray alloc] init];
    }
    else if ([elementName isEqualToString:@"fault"]) {
        faulted = YES;
    }
    else if ([elementName isEqualToString:@"struct"]) {
        [parsingContexts addObject:kFCXMLRPCRequest_StructStart];
    }
    else if ([elementName isEqualToString:@"array"]) {
        [parsingContexts addObject:kFCXMLRPCRequest_ArrayStart];
    }
    else if ([elementName isEqualToString:@"name"]) {
        [parsingContexts addObject:kFCXMLRPCRequest_StructName];
    }

    [stringBuilder release];
    stringBuilder = nil;
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    if (stringBuilder) {
        NSString *trimmed = [stringBuilder stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if ([trimmed length]) {
            [parsingContexts addObject:trimmed];
        }
        stringBuilder = ([stringBuilder release], nil);
    }

    if ([elementName isEqualToString:@"array"]) {
        NSMutableArray *array = [[NSMutableArray alloc] init];

        int count = [parsingContexts count];
        int arrayStart = count - 1;

        while ([parsingContexts objectAtIndex:arrayStart] != kFCXMLRPCRequest_ArrayStart) {
            arrayStart--;
        }

        // TODO: look for API copy
        for (int i = arrayStart+1; i < count; i++) {
            [array addObject:[parsingContexts objectAtIndex:i]];
        }

        [parsingContexts removeObjectsInRange:NSMakeRange(arrayStart,count-arrayStart)];
        [parsingContexts addObject:array];
        [array release];
    }
    else if ([elementName isEqualToString:@"struct"]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];

        int count = [parsingContexts count];
        int structStart = count - 1;

        while ([parsingContexts objectAtIndex:structStart] != kFCXMLRPCRequest_StructStart) {
            structStart--;
        }

        for (int i = structStart+1; i < count; i += 3) {
            // { 'name', name, value }
            // { value, 'name', name }

            int keyPos, valuePos;
            if ([parsingContexts objectAtIndex:i] == kFCXMLRPCRequest_StructName) {
                keyPos = i + 1;
                valuePos = i + 2;
            }
            else {
                keyPos = i + 2;
                valuePos = i;
            }

            [dict setObject:[parsingContexts objectAtIndex:valuePos]
                     forKey:[parsingContexts objectAtIndex:keyPos]];
        }

        [parsingContexts removeObjectsInRange:NSMakeRange(structStart,count-structStart)];
        [parsingContexts addObject:dict];
        [dict release];
    }
    else if ([elementName isEqualToString:@"i4"] || [elementName isEqualToString:@"int"]) {
        NSString *value = [parsingContexts lastObject];
        [parsingContexts removeLastObject];
        [parsingContexts addObject:[NSNumber numberWithInt:value.intValue]];
    }
    else if ([elementName isEqualToString:@"methodResponse"]) {
        response = [[parsingContexts lastObject] retain];
        parsingContexts = ([parsingContexts release], nil);
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    if (!stringBuilder) {
        stringBuilder = [[NSMutableString alloc] init];
    }
    [stringBuilder appendString:string];
}


- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    NSLog(@"error while parsing data:%@", [[[NSString alloc] initWithBytes:[responseBuffer bytes]
                                                                    length:[responseBuffer length]
                                                                  encoding:NSASCIIStringEncoding] autorelease]);
    [delegate requestFailed:self withError:parseError];
    [self autorelease];
}

@end
