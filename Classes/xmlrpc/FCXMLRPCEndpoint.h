#import <Foundation/Foundation.h>

@class FCXMLRPCRequest;

@interface FCXMLRPCEndpoint : NSObject {
    NSURL *endpointURL;
    id delegate;
}

@property (nonatomic, assign) id delegate;
@property (readonly) NSURL *endpointURL;

- (FCXMLRPCRequest*) sendRequestForMethod:(NSString*)methodName
                               withParams:(NSArray*)params;

- (FCXMLRPCRequest*) requestForMethod:(NSString*)methodName
                           withParams:(NSArray*)theParams;


- (id)initWithEndpointURL:(NSURL*)anEndpointURL delegate:(id)delegate;


@end

