@protocol FCXMLRPCRequest;

@protocol FCXMLRPCEndpointDelegate

- (void)requestFinished:(id<FCXMLRPCRequest>)request withResult:(id)result;
- (void)requestFaulted:(id<FCXMLRPCRequest>)request withResult:(id)result;
- (void)requestFailed:(id<FCXMLRPCRequest>)request withError:(NSError*)error;

@end