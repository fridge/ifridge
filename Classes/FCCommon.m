#import "FCCommon.h"

#import "FCFridgeViewController.h"

@implementation FCCommon

+ (NSString*)formatCurrency:(int)dollarsAndCents; {
    return [NSString stringWithFormat:@"$%i.%02i", dollarsAndCents / 100, abs(dollarsAndCents % 100)];
}

+ (void)showFail:(NSString*)title message:(NSString*)message {
    UIAlertView *failView =
	    [[[UIAlertView alloc] initWithTitle:title
                                    message:message
                                   delegate:nil
                          cancelButtonTitle:@"Dismiss"
                          otherButtonTitles:nil] autorelease];
    [failView show];
}

@end
