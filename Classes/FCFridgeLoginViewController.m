#import "FCFridgeLoginViewController.h"

#import "FCCommon.h"
#import "FCProtocolHelper.h"
#import "FCFridgeViewController.h"
#import "FCAuthenticatedRequest.h"

static NSString *kFCFridgeLoginViewController_EndpointURL =
	@"http://localhost:3000/fridgeserver.php";

@implementation FCFridgeLoginViewController

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)viewDidAppear:(BOOL)animated {
    [usernameView becomeFirstResponder];
}

- (IBAction)login:(id)sender {
    self.view.userInteractionEnabled = NO;
    [endpoint release];

    endpoint = [[FCProtocolHelper alloc] initWithEndpointURL:[NSURL URLWithString:kFCFridgeLoginViewController_EndpointURL]
                                                    delegate:self];
    endpoint.username = usernameView.text;
    endpoint.password = passwordView.text;
    [endpoint balance];
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [endpoint release];
    [super dealloc];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == usernameView) {
        [passwordView becomeFirstResponder];
    }
    else if (textField == passwordView) {
        [self login:textField];
    }
    return NO;
}

#pragma mark FCXMLRPCEndpoint Delegate Methods

- (void)requestFinished:(id<FCXMLRPCRequest>)request withResult:(id)result {
    self.view.userInteractionEnabled = YES;

    FCFridgeViewController *fridgeViewController =
    [[FCFridgeViewController alloc] initWithNibName:@"FCFridgeViewController"
                                             bundle:nil
                                        endpointURL:endpoint.endpointURL
                                           username:endpoint.username
                                           password:endpoint.password];

    [self.navigationController pushViewController:fridgeViewController
                                         animated:YES];
    [fridgeViewController release];
}

- (void)requestFaulted:(id<FCXMLRPCRequest>)request withResult:(id)result {
    self.view.userInteractionEnabled = YES;
    [FCCommon showFail:@"Faulted!" message:[result objectForKey:@"faultString"]];
}

- (void)requestFailed:(id<FCXMLRPCRequest>)request withError:(NSError*)error {
    self.view.userInteractionEnabled = YES;
    [FCCommon showFail:@"Failed" message:[error description]];
}



@end
