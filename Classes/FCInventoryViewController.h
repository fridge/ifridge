#import <UIKit/UIKit.h>

#import "FCXMLRPC.h"

@class FCFridgeViewController;
@class FCProtocolHelper;

@interface FCInventoryViewController : UITableViewController <FCXMLRPCEndpointDelegate> {
    FCFridgeViewController *fridgeViewController;
    FCProtocolHelper *endpoint;

    NSMutableArray *categories;
}

@property (nonatomic, assign) FCFridgeViewController *fridgeViewController;
@property (nonatomic, retain) FCProtocolHelper *endpoint;

@end
