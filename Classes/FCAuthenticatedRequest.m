#import "FCAuthenticatedRequest.h"

#import "FCXMLRPC.h"

#import "FCProtocolHelper.h"

@implementation FCAuthenticatedRequest

@synthesize delegate;
@synthesize serverNonce;
@synthesize nonceRequest, innerRequest;

- (FCXMLRPCRequest*)requestForCurrentState {
    switch (state) {
        case FCAuthenticatedRequestStateInner:
            return self.innerRequest;
        case FCAuthenticatedRequestStateNonce:
            return self.nonceRequest;
        default:
            [NSException raise:@"IllegalStateException" format:@"%@ in invalid state", self];
            return nil;
    }
}

- (id)initWithMethodName:(NSString*)aMethodName
                  params:(NSArray*)someParams
          protocolHelper:(FCProtocolHelper*)aHelper
                delegate:(id)aDelegate
{
    if (self = [super init]) {
        helper = [aHelper retain];

        state = FCAuthenticatedRequestStateNonce;
        clientNonce = [[FCProtocolHelper clientNonce] retain];
        self.nonceRequest = [helper requestNonceWithClientNonce:clientNonce];
        self.innerRequest = [helper requestForMethod:aMethodName withParams:someParams];
        nonceRequest.delegate = self;
        innerRequest.delegate = self;
        self.delegate = aDelegate;
    }
    return self;
}

- (void)send {
    [self retain];
    [nonceRequest send];
}

- (void)cancel {
    [[self requestForCurrentState] cancel];
}

- (void)dealloc {
    [nonceRequest cancel];
    self.nonceRequest = nil;
    self.innerRequest = nil;

    helper = ([helper release], nil);
    clientNonce = ([clientNonce release], nil);
    serverNonce = ([serverNonce release], nil);

    [super dealloc];
}

#pragma mark FCXMLRPCEndpoint Delegate Methods

- (void)nonceRequestFinished:(FCXMLRPCRequest*)request withResult:(id)result {
    assert(nonceRequest == request);
    self.nonceRequest = nil;

    serverNonce = [[result objectForKey:@"nonce"] retain];

    NSString *hmacExpected = [helper getAuth:[NSArray arrayWithObjects:serverNonce, clientNonce, nil]];
    NSString *hmacGiven    = [result objectForKey:@"hmac"];

    if ([hmacExpected isEqualToString:hmacGiven]) {
        state = FCAuthenticatedRequestStateInner;

        NSMutableArray *innerParams = innerRequest.params;
        [innerParams insertObject:serverNonce atIndex:0];
        [helper appendAuth:innerParams];
        [innerRequest send];
    }
    else {
        [delegate requestFailed:self withError:[NSError errorWithDomain:@"FCAuthenticatedRequest" code:1 userInfo:nil]];
        [self autorelease];
    }
}

- (void)innerRequestFinished:(FCXMLRPCRequest*)request withResult:(id)result {
    assert(innerRequest == request);
    self.innerRequest = nil;

    NSMutableArray *orderedResponseValues = [NSMutableArray arrayWithObject:serverNonce];
    NSMutableArray *responseKeys = [[result allKeys] mutableCopy];
    [responseKeys removeObject:@"hmac"];
    [responseKeys sortUsingSelector:@selector(compare:)];
    for (NSString *resultKey in responseKeys) {
        [orderedResponseValues addObject:[result objectForKey:resultKey]];
    }

    NSString *hmacGiven = [result objectForKey:@"hmac"];
    NSString *hmacExpected = [helper getAuth:orderedResponseValues];

    if ([hmacGiven isEqualToString:hmacExpected]) {
        [delegate requestFinished:self withResult:result];
    }
    else {
        [delegate requestFailed:self withError:[NSError errorWithDomain:@"FCAuthenticatedRequest" code:2 userInfo:nil]];
    }
    [self autorelease];
}

- (void)requestFinished:(FCXMLRPCRequest*)request withResult:(id)result {
    switch (state) {
        case FCAuthenticatedRequestStateInner:
            [self innerRequestFinished:request withResult:result];
            break;
        case FCAuthenticatedRequestStateNonce:
            [self nonceRequestFinished:request withResult:result];
            break;
    }
}

- (void)requestFaulted:(FCXMLRPCRequest*)request withResult:(id)result {
    assert(nonceRequest == request || innerRequest == request);
    self.nonceRequest = nil;
    self.innerRequest = nil;

    [delegate requestFaulted:self withResult:result];
    [self autorelease];
}

- (void)requestFailed:(FCXMLRPCRequest*)request withError:(NSError*)error {
    assert(nonceRequest == request);
    self.nonceRequest = nil;
    self.innerRequest = nil;

    [delegate requestFailed:self withError:error];
    [self autorelease];
}

- (NSString*)methodName {
    return innerRequest.methodName;
}

- (void)setMethodName:(NSString *)aMethodName {
    innerRequest.methodName = aMethodName;
}

- (NSMutableArray*)params {
    return innerRequest.params;
}

- (void)setParams:(NSMutableArray *)someParams {
    innerRequest.params = someParams;
}

@end
