#import "FCInventoryCategory.h"

@implementation FCInventoryCategory

@synthesize displaySequence, title, products;

- (id)init {
    if (self = [super init]) {
        products = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)dealloc {
    products = ([products release], nil);
    [super dealloc];
}

- (NSComparisonResult)compareDisplaySequence:(FCInventoryCategory*)other {
    int mine = self.displaySequence;
    int theirs = other.displaySequence;
    if (mine == theirs) {
        return NSOrderedSame;
    }
    else if (mine < theirs) {
        return NSOrderedAscending;
    }
    else {
        return NSOrderedDescending;
    }
}

@end
