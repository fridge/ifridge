#import "FCInventoryViewController.h"

#import "FCCommon.h"
#import "FCInventoryCategory.h"
#import "FCProtocolHelper.h"
#import "FCFridgeViewController.h"

@implementation FCInventoryViewController

@synthesize fridgeViewController;
@synthesize endpoint;

/*
- (id)initWithStyle:(UITableViewStyle)style {
    // Override initWithStyle: if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
    if (self = [super initWithStyle:style]) {
    }
    return self;
}
*/


- (void)viewDidLoad {
    [super viewDidLoad];

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;


    if (!endpoint) {
        endpoint = [[FCProtocolHelper alloc] initWithEndpointURL:fridgeViewController.endpointURL
                                                        delegate:self];
    }
    if (!categories) {
        categories = [[NSMutableArray alloc] init];
    }

    // Just loaded. Attempt to refresh product list.
    [endpoint getStockForUser:fridgeViewController.username];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;

//    [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];

//    [urlRequest release];
//    [urlConnection release];

}


/*
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}
*/
/*
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}
*/
/*
- (void)viewWillDisappear:(BOOL)animated {
	[super viewWillDisappear:animated];
}
*/
/*
- (void)viewDidDisappear:(BOOL)animated {
	[super viewDidDisappear:animated];
}
*/

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


#pragma mark Table view methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [categories count];
}


// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    FCInventoryCategory *category = [categories objectAtIndex:section];
    return [category.products count];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"Cell";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier] autorelease];
    }

    FCInventoryCategory *category = [categories objectAtIndex:indexPath.section];
    NSDictionary *productInfo = [category.products objectAtIndex:indexPath.row];

    cell.textLabel.text = [productInfo objectForKey:@"description"];


    int dollarsAndCents = [[productInfo objectForKey:@"price"] intValue];
    cell.detailTextLabel.text = [FCCommon formatCurrency:dollarsAndCents];

    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    FCInventoryCategory *category = [categories objectAtIndex:section];
    return category.title;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Navigation logic may go here. Create and push another view controller.
	// AnotherViewController *anotherViewController = [[AnotherViewController alloc] initWithNibName:@"AnotherView" bundle:nil];
	// [self.navigationController pushViewController:anotherViewController];
	// [anotherViewController release];
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/


/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }
}
*/


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/


/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


- (void)dealloc {
    categories = ([categories release], nil);
    endpoint = ([endpoint release], nil);

    [super dealloc];
}


#pragma mark FCXMLRPCEndpoint Delegate Methods

- (void)requestFinished:(id<FCXMLRPCRequest>)request withResult:(id)result {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    NSLog(@"request %@ finished, result=%@", request, result);

    NSMutableDictionary *categoriesMap = [[NSMutableDictionary alloc] init];

    for (NSDictionary *productInfo in result) {
        NSString *categoryName = [productInfo objectForKey:@"category"];
        FCInventoryCategory *category = [categoriesMap objectForKey:categoryName];
        if (!category) {
            category = [[FCInventoryCategory alloc] init];
            category.title = categoryName;
            category.displaySequence = [[productInfo objectForKey:@"category_order"] intValue];

            [categories addObject:category];
            [categoriesMap setObject:category forKey:categoryName];

            [category release];
        }
        [category.products addObject:productInfo];
    }
    [categoriesMap release];

    NSSortDescriptor *sortByName =
    	[[[NSSortDescriptor alloc] initWithKey:@"description" ascending:YES] autorelease];
    NSArray *productOrdering = [NSArray arrayWithObject:sortByName];
    for (FCInventoryCategory *category in categories) {
        [category.products sortUsingDescriptors:productOrdering];
    }

    [categories sortUsingSelector:@selector(compareDisplaySequence:)];
    [self.tableView reloadData]; // TODO: hmm

}

- (void)requestFaulted:(id<FCXMLRPCRequest>)request withResult:(id)result {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [FCCommon showFail:@"Faulted" message:[result objectForKey:@"faultString"]];
}

- (void)requestFailed:(id<FCXMLRPCRequest>)request withError:(NSError*)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [FCCommon showFail:@"Failed" message:[error description]];
}

@end

