#import "FCAccountViewController.h"

#import "FCAuthenticatedRequest.h"
#import "FCProtocolHelper.h"
#import "FCCommon.h"
#import "FCFridgeViewController.h"

@implementation FCAccountViewController

@synthesize fridgeViewController;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

    if (!endpoint) {
        endpoint = [[FCProtocolHelper alloc] initWithEndpointURL:fridgeViewController.endpointURL
                                                        delegate:self];
        endpoint.username = fridgeViewController.username;
        endpoint.password = fridgeViewController.password;
    }

    [endpoint balance];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    endpoint = ([endpoint release], nil);
    [super dealloc];
}

#pragma mark FCXMLRPCEndpoint Delegate Methods

- (void)requestFinished:(id<FCXMLRPCRequest>)request withResult:(id)result {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    NSNumber *balance = [result objectForKey:@"balance"];
    balanceLabel.text = [FCCommon formatCurrency:balance.intValue];
}

- (void)requestFaulted:(id<FCXMLRPCRequest>)request withResult:(id)result {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    [FCCommon showFail:@"Faulted"
               message:[result objectForKey:@"faultString"]];

}
- (void)requestFailed:(id<FCXMLRPCRequest>)request withError:(NSError*)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;

    [FCCommon showFail:@"Failed"
               message:[error description]];
}

@end
