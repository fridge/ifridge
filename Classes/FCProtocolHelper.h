#import <Foundation/Foundation.h>

#import "FCXMLRPC.h"

@interface FCProtocolHelper : FCXMLRPCEndpoint {
    NSString *username;
    NSString *password;
}

@property (nonatomic, retain) NSString *username;
@property (nonatomic, retain) NSString *password;


+ (NSString*)clientNonce;

- (NSString*)getAuth:(NSArray*)params;
- (NSMutableArray*)appendAuth:(NSMutableArray*)params;

- (FCXMLRPCRequest*)sendAuthenticatedRequestForMethod:(NSString*)methodName
                                           withParams:(NSArray*)params;

- (FCXMLRPCRequest*)requestNonceWithClientNonce:(NSString*)clientNonce;

- (FCXMLRPCRequest*)balance;

- (FCXMLRPCRequest*)transfer:(int)amount
                        from:(NSString*)fromUser
                          to:(NSString*)toUser;

- (FCXMLRPCRequest*)getStock;
- (FCXMLRPCRequest*)getStockForUser:(NSString*)aUsername;

@end
