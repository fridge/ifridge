#import "FCFridgeViewController.h"


@implementation FCFridgeViewController

@synthesize username, password, endpointURL;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          endpointURL:(NSURL*)anEndpointURL
             username:(NSString*)aUsername
             password:(NSString*)aPassword
{
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        endpointURL = [anEndpointURL retain];
        username = [aUsername copy];
        password = [aPassword copy];
    }
    return self;
}


/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    tabBar.view.frame = self.view.bounds;
    [self.view addSubview:tabBar.view];

    self.navigationItem.title = username;

    for (UIViewController *vc in tabBar.viewControllers) {
        if ([vc respondsToSelector:@selector(setFridgeViewController:)]) {
            [vc performSelector:@selector(setFridgeViewController:) withObject:self];
        }
    }
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [endpointURL release];
    [username release];
    [password release];
    [super dealloc];
}


@end
