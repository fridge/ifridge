#import <UIKit/UIKit.h>

#import "FCXMLRPC.h"

@class FCProtocolHelper;

@interface FCFridgeLoginViewController : UIViewController<UITextFieldDelegate,FCXMLRPCEndpointDelegate> {
    IBOutlet UITextField *usernameView;
    IBOutlet UITextField *passwordView;

    FCProtocolHelper *endpoint;
}

- (IBAction)login:(id)sender;

@end
