#import <Foundation/Foundation.h>

@class FCFridgeViewController;

@interface FCCommon : NSObject {

}

+ (NSString*)formatCurrency:(int)dollarsAndCents;

+ (void)showFail:(NSString*)title message:(NSString*)message;

@end
