#include <sys/time.h>

#include <CommonCrypto/CommonDigest.h>
#include <CommonCrypto/CommonHMAC.h>

#import "FCProtocolHelper.h"

#import "FCAuthenticatedRequest.h"


const static int kFCProtocolHelper_NonceLength = 20;
static NSString *kFCProtocolHelper_NonceCharacters = @"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

@implementation FCProtocolHelper

@synthesize username, password;

+ (NSString*)bytesToHexString:(UInt8*)md5 length:(NSUInteger)len {
    NSMutableString *str = [NSMutableString stringWithCapacity:len*2];
    for (int i = 0; i < len; i++) {
        [str appendFormat:@"%02x", md5[i]];
    }
    return str;
}

// TODO: How did this turn out so nasty? :-(
+ (NSString*)clientNonce {
    char nonce[kFCProtocolHelper_NonceLength+1];
    nonce[kFCProtocolHelper_NonceLength] = 0;

    int max = [kFCProtocolHelper_NonceCharacters length];
    for (int i = 0; i < kFCProtocolHelper_NonceLength; i++) {
        int index = random() % max;
        nonce[i] = [kFCProtocolHelper_NonceCharacters characterAtIndex:index];
    }
    return [NSString stringWithCString:nonce encoding:NSASCIIStringEncoding];
}

- (NSString*)getAuth:(NSArray*)params {
    NSMutableString *request = [NSMutableString string];
    BOOL first = YES;
    for (id p in params) {
        if (first) {
            first = NO;
        }
        else {
            [request appendString:@","];
        }

        NSString *str;
        if ([p respondsToSelector:@selector(stringValue)]) {
            str = [p stringValue];
        } else {
            str = p;
        }
        [request appendString:str];
    }
    NSData *rawrequest = [request dataUsingEncoding:NSASCIIStringEncoding];

    if (!rawrequest) {
        [NSException raise:@"FCProtcolHelperEncodingError"
                    format:@"Request could not be encoded as ASCII"];
    }

    NSData *rawpassword;
    {
        UInt8 pass_md5[16];
        NSData *passdata = [password dataUsingEncoding:NSASCIIStringEncoding];
        CC_MD5((const void*)[passdata bytes], [passdata length], pass_md5);
        rawpassword = [[FCProtocolHelper bytesToHexString:pass_md5 length:sizeof(pass_md5)]
                       dataUsingEncoding:NSASCIIStringEncoding];
    }

    UInt8 hmac[16];
    CCHmac(kCCHmacAlgMD5,
           [rawpassword bytes], [rawpassword length],
           [rawrequest bytes], [rawrequest length],
           &hmac);

    return [FCProtocolHelper bytesToHexString:hmac length:16];
}

- (NSMutableArray*)appendAuth:(NSMutableArray*)params {
    [params addObject:[self getAuth:params]];
    return params;
}

- (id<FCXMLRPCRequest>)sendAuthenticatedRequestForMethod:(NSString*)methodName
                                              withParams:(NSArray*)params {
    id<FCXMLRPCRequest> request = [[[FCAuthenticatedRequest alloc] initWithMethodName:methodName
                                                                               params:params
                                                                       protocolHelper:self
                                                                             delegate:delegate] autorelease];
    [request send];
    return request;
}

- (id<FCXMLRPCRequest>)requestNonceWithClientNonce:(NSString*)clientNonce {
    struct timeval tv;
    gettimeofday(&tv, NULL);

    NSMutableArray *params = [NSMutableArray arrayWithObjects:
                              clientNonce,
                              [NSNumber numberWithInt:tv.tv_sec],
                              username,
                              nil];

    return [self requestForMethod:@"generate_nonce"
                       withParams:[self appendAuth:params]];
}

- (id<FCXMLRPCRequest>)balance {
    return [self transfer:0 from:username to:username];
}

- (id<FCXMLRPCRequest>)transfer:(int)amount
            from:(NSString*)fromUser
              to:(NSString*)toUser
{
    return [self sendAuthenticatedRequestForMethod:@"transfer"
                                        withParams:[NSMutableArray arrayWithObjects:
                                                    fromUser,
                                                    toUser,
                                                    [NSNumber numberWithInt:amount],
                                                    nil]];
}


- (id<FCXMLRPCRequest>)getStock {
    return [self sendRequestForMethod:@"get_stock" withParams:nil];
}

- (id<FCXMLRPCRequest>)getStockForUser:(NSString*)aUsername {
    return [self sendRequestForMethod:@"get_stock" withParams:
            [NSArray arrayWithObject:aUsername]];
}


@end
