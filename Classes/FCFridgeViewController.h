#import <UIKit/UIKit.h>

@interface FCFridgeViewController : UIViewController {
    IBOutlet UITabBarController *tabBar;

    NSURL *endpointURL;
    NSString *username;
    NSString *password;
}

@property (readonly) NSURL *endpointURL;
@property (readonly) NSString *username;
@property (readonly) NSString *password;

- (id)initWithNibName:(NSString *)nibNameOrNil
               bundle:(NSBundle *)nibBundleOrNil
          endpointURL:(NSURL*)anEndpointURL
             username:(NSString*)aUsername
             password:(NSString*)aPassword;

@end
